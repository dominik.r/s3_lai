iso, vol, geo = 1.0, 0.189184, -1.377622

# Broadband albedo visible
vis_bb_17 = 0.0471596  # constant for band17
vis_bb_08 = 0.648476   # constant for band8

# Broadband albedo NIR
nir_bb_17 = 0.337262  # constant for band17
nir_bb_08 = 1.09514   # constant for band8
