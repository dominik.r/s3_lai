"""
integrate.py
Contains functions for angular and spectral integration

useful papers:
http://www2.geog.ucl.ac.uk/~plewis/burnscar/igarss04.pdf
https://ieeexplore.ieee.org/document/5471049

"""

import os
from coefficients import *

import matplotlib.pyplot as plt
import xarray as xr


def main():
    root = '/home/woozle/Work/ET-Sense/GLEAM/Observations/S3_LAI/transfer_20feb2020/datasets/'

    # read all data into single xarray.Dataset
    ds_list = []

    for filename in os.listdir(root):
        if filename.endswith("REBELS.nc"):
            site_id = filename.split('_')[0]
            ds = xr.open_dataset(root + filename)

            ds = ds.assign_coords(z=site_id)
            ds_list.append(ds)

    dsx = xr.concat(ds_list, dim='z')

    # b08 = int_angular(dsx, 'Oa08_toc', iso, vol, geo)
    # b17 = int_angular(dsx, 'Oa17_toc', iso, vol, geo)

    # b08.plot.hist(bins=50)
    # b17.plot.hist(bins=50)

    # b08_17 = xr.concat([b08, b17], dim='band')
    # b08_17.plot.scatter(x='0a08_toc', y='0a17_toc')

    bb_vis = int_spectral(int_angular(dsx, 'Oa08_toc', iso, vol, geo),
                          int_angular(dsx, 'Oa17_toc', iso, vol, geo),
                          vis_bb_08, vis_bb_17)

    bb_nir = int_spectral(int_angular(dsx, 'Oa08_toc', iso, vol, geo),
                          int_angular(dsx, 'Oa17_toc', iso, vol, geo),
                          nir_bb_08, nir_bb_17)

    bb_vis.plot.hist(bins=50)
    bb_nir.plot.hist(bins=50)

    plt.scatter(bb_vis, bb_nir, s=0.5)


def int_angular(dsx, band, iso, vol, geo):
    """
    Angular integration returning white sky albedo

    Parameters
    ----------
    dsx : xarray.dataArray
        contains reflectances and kernel weights
    band : str
        Sentinel 3 band
    iso : numpy.float
        Coefficient for isotropic scattering
    vol : numpy.float
        Coefficient for single scattering
    geo : numpy.float
        Coefficient for shadowing effects

    Returns
    -------
    bhr :xarray.DataArray
        white sky albedo / bi-hemispherical reflectance

    """

    #     f_iso : xarray.DataArray
    #         Kernel weights for isotropic scattering from surface
    #     f_vol : xarray.DataArray
    #         Kernel weights for single scattering approximation to radiative transfer
    #     f_geo : xarray.DataArray
    #         Kernel weights for shadowing effects

    iso = xr.DataArray(iso)
    vol = xr.DataArray(vol)
    geo = xr.DataArray(geo)

    bhr = xr.dot(dsx.sel(band=band)['kiso'], iso) + \
          xr.dot(dsx.sel(band=band)['kvol'], vol) + \
          xr.dot(dsx.sel(band=band)['kgeo'], geo)

    return bhr


def int_spectral(bhr1, bhr2, c1, c2):
    """
    Spectral integration returning broadband albedo

    Parameters
    ----------
    bhr1 : xarray.DataArray
        bi-hemispherical reflectance for first band

    bhr2 : xarray.DataArray
        bi-hemispherical reflectance for second band

    c1 : numpy.float
        coefficient for first band

    c2 : numpy.float
        coefficient for second band

    Returns
    -------
    bb :xarray.DataArray
        broadband albedo

    """

    c1 = xr.DataArray(c1)
    c2 = xr.DataArray(c2)

    bb = xr.dot(bhr1, c1) + xr.dot(bhr2, c2)

    return bb
